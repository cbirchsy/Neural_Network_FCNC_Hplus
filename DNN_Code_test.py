#!/bin/python
from optparse import OptionParser
import os
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from pandas.plotting import scatter_matrix

import sklearn.utils
from sklearn.preprocessing import StandardScaler,LabelEncoder
from sklearn.model_selection import train_test_split

from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Input, BatchNormalization
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers.core import Dense, Activation
from keras import optimizers
from prettytable import PrettyTable

#****************************************
#Adding Options when runing the code. 
#****************************************
def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

def plot_distribution(pd_plot_sig, pd_plot_bkg, varlist, plot_folder):
    for var in varlist:
        plot_name=plot_folder+'/%s.png' % var
        if(var.find('_btagw')!=-1):
            bins = np.linspace(-1, 1, 11)
        else:
            bins = np.linspace(min(pd_plot_bkg[var]), max(pd_plot_bkg[var]), 50)
        plt.hist(pd_plot_bkg[var], histtype='step', density=True, bins=bins, label='$ttbar$', linewidth=2)
        plt.hist(pd_plot_sig[var], histtype='step', density=True, bins=bins, label='$H^+ cb, m_H=100GeV$', linewidth=2)
        plt.xlabel(var)
        plt.ylabel('Abitrary Units')
        plt.legend(loc='best')
        plt.savefig(plot_name)
        plt.clf()
    return

def plot_correlations(pd_plot, varlist, plot_folder, plot_name):
    plot_name=plot_folder+"/"+plot_name+".png"
    correlations = pd_plot[varlist].corr()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlations, vmin=-1, vmax=1, cmap=cm.coolwarm)
    fig.colorbar(cax)
    ticks = np.arange(0,len(varlist),1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(varlist) 
    ax.set_yticklabels(varlist) 
    ax.tick_params(top=False, bottom=True,labeltop=False, labelbottom=True)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor") 
    fig.tight_layout()
    plt.savefig(plot_name)
    plt.clf()
    return

def plot_scatter(pd_plot, varlist, plot_folder, plot_name, size):
    Input_cat=[]
    for i in range(0,len(varlist)-size+1, size):
        InputVariables_tmp=varlist[i:i+size]
        Input_cat.append(InputVariables_tmp)
    plot_name_tmp=plot_folder+"/"+plot_name
    
    for i in range(len(Input_cat)-1):
        for j in range(i+1, len(Input_cat)):
            Scatter_var=Input_cat[i]+Input_cat[j]
            scatter_matrix(pd_plot[Scatter_var], figsize=(18,18))
            plot_name_n=plot_name_tmp+'_%i%i.png' %(i,j)
            plt.savefig(plot_name_n)
            plt.clf()

def Randomizing(df):
    df = sklearn.utils.shuffle(df,random_state=123) #'123' is the random seed
    df = df.reset_index(drop=True)# drop=True does not allow the reset_index to insert a new column with the suffled index                                                                                
    return df


def BuildDNN(N_input, width, depth, activation , dropout, do_bn):
    model = Sequential()
    model.add(Dense(units=width, input_dim=N_input))
    if do_bn:
       model.add(BatchNormalization(momentum=0.99, epsilon=0.001))
    model.add(Activation(activation))
    model.add(Dropout(dropout))

    for i in xrange(0, depth):
        model.add(Dense(width))
        if do_bn:
            model.add(BatchNormalization(momentum=0.99, epsilon=0.001))
        model.add(Activation(activation))
        # Dropout randomly sets a fraction of input units to 0 at each update during training time                                                                                                                  
        # which helps prevent overfitting.                                                                                                                                                                 
        model.add(Dropout(dropout))

    model.add(Dense(1, activation='sigmoid'))

    return model


#######################################
parser = OptionParser()
parser.add_option('-s', '--SigList',
                  type='string',
                  help='list of signal, separate with comma, no blanck space', 
                  action='callback',
                  callback=get_comma_separated_args,
                  dest = "SigList")
parser.add_option('-b', '--BkgList',
                  type='string',
                  help='list of background, separate with comma, no blanck space',
                  action='callback',
                  callback=get_comma_separated_args,
                  dest = "BkgList")
parser.add_option('-p', '--filepath',
                  type='string',
                  help='Input File path, default to /eos/atlas/user/p/pengc/Hplus_Data_pkl/',
                  default='/eos/atlas/user/p/pengc/Hplus_Data_pkl/',)

parser.add_option("--do_plots", action="store_true", dest="do_plots", help="do variable plots, True or False, using default set to false if without -r" , default=False)

parser.add_option("--plot_dir", dest="plot_dir", type='string', help='directory perfix to store plots', default="plots")
parser.add_option("-f", "--files_dir", dest="files_dir", type='string', help='Files saved folder', default="Files")
parser.add_option("-r", "--read", dest="Read", action="store_true", help="-r read full data sample, may need wait for long time", default=False)
parser.add_option("-a", "--activation", dest="ACTIVATION", type='string', help="activation function", default='relu')
parser.add_option("--lr", dest="LR", help="learning rate", type='float', default=0.001)
parser.add_option("--do_batchnorm", dest="do_BN", action="store_false", help="Use batch normalizetion", default=True)
parser.add_option("--dropout", dest='DropOut', type='float', help='Drop out', default=0.5)
parser.add_option("-o", "--optimazer", dest="OPTIMAZER", help="optimazer used to minimaze loss function, See keras documentation", type='string', default='adam')
parser.add_option("--batchsize", dest="BatchSize", help="Batch_size", type='int', default=1024)
(options, args) = parser.parse_args()

#****************************************
#Data Processing: Getting signal and background sample
#****************************************
InputVariables = ['jet0_btagw', 'jet1_btagw', 'jet2_btagw', 'jet3_btagw', 'jet0_m', 'jet1_m','jet2_m', 'jet3_m','jet0_phi', 'jet1_phi', 'jet2_phi', 'jet3_phi', 'jet0_pt', 'jet1_pt','jet2_pt', 'jet3_pt', 'jet0_Rapidity', 'jet1_Rapidity', 'jet2_Rapidity', 'jet3_Rapidity']
if options.SigList == None: 
    siglist=['uHbW']
else:
    siglist=options.SigList
    
if options.BkgList == None: 
    bkglist=['ttbarbb']
else:
    bkglist=options.BkgList
files_folder=options.files_dir+"_"+siglist[0]

if options.Read:
    def GetSamples(samples, samplepath):
        pd_data=pd.DataFrame()
        for sample in samples:
            print '==>Loading '+sample
            File=samplepath+"%s.pkl" % sample
            df_tmp = pd.read_pickle(File)
            pd_data = pd.concat([pd_data, df_tmp], ignore_index=True)
        return pd_data
    
    TotalPD_sig_tmp=GetSamples(siglist, options.filepath)
    TotalPD_bkg_tmp=GetSamples(bkglist, options.filepath)
    
    #evnet selection based on jets number and b-jets number
    Total_PD_sig=TotalPD_sig_tmp[TotalPD_sig_tmp.bjets_n==3]
    TotalPD_sig=Total_PD_sig[Total_PD_sig.jets_n==5]
    Total_PD_bkg=TotalPD_bkg_tmp[TotalPD_bkg_tmp.bjets_n==3]
    TotalPD_bkg=Total_PD_bkg[Total_PD_bkg.jets_n==5]
    
    print "==>Total Signal evnets: ", TotalPD_sig.shape[0]
    print "==>Total Background events: ", TotalPD_bkg.shape[0]

#Data Ploting (Distribution, correletion, scatter plot)
    
    if options.do_plots:
       print "==> Ploting variable distribution, correlations and scatter plots"
       plot_dir=options.plot_dir+"_"+siglist[0]
       print "  --> creating output plot folder " + plot_dir
       os.system("mkdir -p " + plot_dir)
       
       plot_distribution(TotalPD_sig, TotalPD_bkg, InputVariables, plot_dir)
       plot_correlations(TotalPD_sig, InputVariables, plot_dir, 'Corelations_S')
       plot_correlations(TotalPD_bkg, InputVariables, plot_dir, 'Corelations_B')
       plot_scatter(TotalPD_sig, InputVariables, plot_dir,'Scatter_S', 4)
       plot_scatter(TotalPD_bkg, InputVariables, plot_dir,'Scatter_B', 4)
     
    
#Suffering Data

    TotalPD_sig = Randomizing(TotalPD_sig)
    TotalPD_bkg = Randomizing(TotalPD_bkg)
    
    frames_2lj = [TotalPD_bkg[:TotalPD_sig.shape[0]],TotalPD_sig]
    #frames_2lj=[TotalPD_bkg, TotalPD_sig] #Full background Statistic
    df_skim_2lj_tmp = pd.concat(frames_2lj,ignore_index=True)
    df_skim_2lj=Randomizing(df_skim_2lj_tmp)

    os.system("mkdir -p "+files_folder)
    df_skim_2lj.to_pickle(files_folder+'/MixData_PD.pkl')
    print "==> Mixed data saved in %s" %files_folder
else:
    print "==> Reading mixed data from %s" %files_folder 
    df_skim_2lj=pd.read_pickle(files_folder+'/MixData_PD.pkl')
print "==>Total events after shuffling: ",df_skim_2lj.shape[0]

#****************************************
#create X array and y array, split train and 
#****************************************
X=df_skim_2lj[InputVariables].as_matrix()
y_tmp = df_skim_2lj['IsSig']
w = df_skim_2lj['FullEventsWeight']
scaler = StandardScaler()
X = scaler.fit_transform(X)
le = LabelEncoder()
y = le.fit_transform(y_tmp)
X_train, X_val, y_train, y_val, w_train, w_val = train_test_split(X, y, w, train_size=0.5,random_state=123)

#****************************************
# Build DNN
#****************************************

#config DNN model
n_dim=X_train.shape[1]
n_nodes = 32
n_depth=2
activation=options.ACTIVATION
do_bn=options.do_BN
activation_f='sigmoid'
batchsize=options.BatchSize
dropout=options.DropOut
optimizer=options.OPTIMAZER
print "==>NN Configration<=="
NN_config=PrettyTable()
NN_config.field_names = ["Options", "Value"]
NN_config.add_row(["Number of hidder layer", n_depth])
NN_config.add_row(["Number of nodes", n_nodes])
NN_config.add_row(["Do batch normalization", do_bn])
NN_config.add_row(["Activation function", activation])
NN_config.add_row(["Activation function for output layer", activation_f])
NN_config.add_row(["Dropout",dropout])
NN_config.add_row(["Optimazer",optimizer])
NN_config.add_row(["Learing rate", options.LR])
NN_config.add_row(["Batch size", batchsize])
print NN_config
plotsuffix='_depth%s_nodes%s_lr%s_bs%s' %(n_depth, n_nodes, options.LR, options.BatchSize)
modelfolder=files_folder+'/model'+plotsuffix
os.system("mkdir -p " + modelfolder)
modelname=modelfolder+'/model.h5'
lossplotname=modelfolder+'/Loss.png'
accplotname=modelfolder+'/Accuracy.png'
#################################################
model = BuildDNN(n_dim, n_nodes, n_depth, activation, 0.5, do_bn)
adam = optimizers.Adam(lr=options.LR, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, amsgrad=False)
model.compile(loss='binary_crossentropy',optimizer=adam ,metrics=['accuracy'])
callbacks = [
# if we don't have an increase of the accuracy for 20 epochs, terminate training.                                                                                                                          
EarlyStopping(verbose=True, patience=20, monitor='val_acc'),
# Always make sure that we're saving the model weights with the best accuracy.                                                                                                                             
ModelCheckpoint(modelname, monitor='val_acc', verbose=True, save_best_only=True, mode='max')
]
# Start the training!                                                                                                                                                                                      
modelMetricsHistory = model.fit(X_train, y_train, epochs=300, batch_size=batchsize, validation_data=(X_val, y_val),callbacks=callbacks, verbose=0)

perf_1 = model.evaluate(X_train, y_train, batch_size=batchsize)
perf_2 = model.evaluate(X_val, y_val, batch_size=batchsize)
testLoss = 'Test loss:%0.3f' % perf_2[0]
testAccuracy = 'Test accuracy:%0.3f' % perf_2[1]
print testLoss
print testAccuracy
print  '==> Done!'


# summarize history for accuracy                                                                                                                                                                            
plt.plot(modelMetricsHistory.history['acc'])
plt.plot(modelMetricsHistory.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Val'], loc='lower right')
plt.figtext(0.5, 0.5, testAccuracy, wrap=True, horizontalalignment='center', fontsize=10)
plt.savefig(accplotname)
plt.clf()
# summarize history for loss                                                                                                                                                                                
plt.plot(modelMetricsHistory.history['loss'])
plt.plot(modelMetricsHistory.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Val'], loc='upper right')
plt.figtext(0.5, 0.5, testLoss, wrap=True, horizontalalignment='center', fontsize=10)
plt.savefig(lossplotname)
plt.clf()

from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report

# Generates output predictions for the input samples.                                                                                                                                                       
#yhat_test = model.predict(X_val,batch_size=batchsize)
yhat_val=model.predict(X_val, batch_size=batchsize)
yhat_train=model.predict(X_train, batch_size=batchsize)

np.save(modelfolder+'/yhat_val', yhat_val)
np.save(modelfolder+'/y_val', y_val)
np.save(modelfolder+'/yhat_train', yhat_train)
np.save(modelfolder+'/y_train', y_train)

print y_val.shape

fpr, tpr, thresholds = roc_curve(y_val, yhat_val)

roc_auc  = auc(fpr, tpr)
print "ROC AUC: %0.3f" % roc_auc

tnr = 1-fpr

plt.plot(tpr, tnr, color='black',  lw=2, label='Testing, ROC curve (area = %0.3f)' % roc_auc)
plt.plot([0, 1], [1, 0], color='navy', lw=2, linestyle='--')
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.ylabel('Background rejection')
plt.xlabel('Signal efficiency')
plt.title('ROC curves for Background rejection vs Signal')
# plt.plot([0.038], [0.45], marker='*', color='red',markersize=5, label="Cut-based",linestyle="None")                                                                                                       
# plt.plot([0.038, 0.038], [0,1], color='red', lw=1, linestyle='--') # same background rejection point                                                                                                      
plt.legend(loc="lower right")
plt.savefig(modelfolder+"/ROC.pdf")
plt.clf()


def split_classifer(y_true, y_prob):
    index = 0
    y_prob_bkg=[]
    y_prob_sig=[]
    for i in y_true:
        if i==0: #bkgevent                                                                                                                                                                                
           y_prob_bkg.append(y_prob[index,0])
        elif i==1: #signalevent                                                                                                                                                                            
           y_prob_sig.append(y_prob[index,0])
        else:
           print 'error'
        index = index+1
    return y_prob_sig, y_prob_bkg

ysig, ybkg=split_classifer(y_val, yhat_val)

print 'bins'
bins = np.linspace(0,1,21)
plt.hist(ysig, histtype='step', density=True, bins=bins, label='$H^+cb,m_H=100GeV$', linewidth=2)
print "1"
plt.hist(ybkg, histtype='step', density=True, bins=bins, label='$ttbar$', linewidth=2)
print '2'
plt.xlabel('Discriminant')
plt.ylabel('Abitrary Units')
plt.legend(loc='best')
print 'saving'
plt.savefig(modelfolder+'/Discriminant.png')
plt.clf()
