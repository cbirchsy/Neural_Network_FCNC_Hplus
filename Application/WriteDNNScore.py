import os, sys
import ROOT
import numpy as np
import h5py
import math

from array import array
from sklearn.preprocessing import StandardScaler,LabelEncoder

inputFile = '/eos/atlas/user/p/pengc/NN_Outputs_09_Oct_2019_FCNC/uHbW.root'

file = ROOT.TFile(inputFile, 'update')

tree = file.Get('tree')
tree_new = file.Get('tree')

NEntries = tree.GetEntries()


print ('Total Entries = ' + str(NEntries))

InputVariables = ['jet3_btagw_discrete', 'jet4_btagw_discrete',  #['jet0_btagw_discrete', 'jet1_btagw_discrete', 'jet2_btagw_discrete', 'jet3_btagw_discrete', 'jet4_btagw_discrete',
		  'jet0_m_bord', 'jet1_m_bord','jet2_m_bord', 'jet3_m_bord', 'jet4_m_bord',
		  'jet0_phi_bord', 'jet1_phi_bord', 'jet2_phi_bord','jet3_phi_bord', 'jet4_phi_bord',
		  'jet0_pt_bord', 'jet1_pt_bord','jet2_pt_bord', 'jet3_pt_bord', 'jet4_pt_bord',
		  'jet0_y_bord', 'jet1_y_bord', 'jet2_y_bord', 'jet3_y_bord', 'jet4_y_bord',
		  'lep1_eta', 'lep1_phi', 'lep1_pt', 'met', 'met_phi',
                  'mH', 'mbb_leading_bjets', 'mbb_maxdr', 'mbb_mindr']

#OtherVariables = ['jets_n', 'bjets_n', 'nomWeight_weight_btag', 'nomWeight_weight_jvt', 'nomWeight_weight_leptonSF', 'nomWeight_weight_mc', 'nomWeight_weight_norm', 'nomWeight_weight_pu',
#                  'nomWeight_bTagSF_DL1_Continuous_eigenvars_B_0_down', 'nomWeight_bTagSF_DL1_Continuous_eigenvars_B_1_down', 'nomWeight_bTagSF_DL1_Continuous_eigenvars_B_0_down']

#AllVariables = InputVariables+OtherVariables

origtree = tree

#tree.SetBranchStatus("*",0)

# -- initiating the relevant branches only ----- #

tree.SetBranchStatus("*", 1)

InputData = np.zeros(shape=(NEntries, len(InputVariables) ))

# -- storing the tree in numpy array -- #

decade = 0
for ientry in range(NEntries) :

	progress = 10.0*ientry / (1*NEntries)
	p = math.floor(progress)
	if(p > decade) : 
            print ( str(10*p) + '%')
	decade = p

	tree.GetEntry(ientry)

#	InputData[ientry][0] = tree.jet0_btagw_discrete
#	InputData[ientry][1] = tree.jet1_btagw_discrete
#	InputData[ientry][0] = tree.jet2_btagw_discrete
	InputData[ientry][0] = tree.jet3_btagw_discrete
	InputData[ientry][1] = tree.jet4_btagw_discrete

	InputData[ientry][2] = tree.jet0_m_bord
	InputData[ientry][3] = tree.jet1_m_bord
	InputData[ientry][4] = tree.jet2_m_bord
	InputData[ientry][5] = tree.jet3_m_bord
	InputData[ientry][6] = tree.jet4_m_bord

	InputData[ientry][7] = tree.jet0_phi_bord
	InputData[ientry][8] = tree.jet1_phi_bord
	InputData[ientry][9] = tree.jet2_phi_bord
	InputData[ientry][10] = tree.jet3_phi_bord
	InputData[ientry][11] = tree.jet4_phi_bord

	InputData[ientry][12] = tree.jet0_pt_bord
	InputData[ientry][13] = tree.jet1_pt_bord
	InputData[ientry][14] = tree.jet2_pt_bord
	InputData[ientry][15] = tree.jet3_pt_bord
	InputData[ientry][16] = tree.jet4_pt_bord

	InputData[ientry][17] = tree.jet0_y_bord
	InputData[ientry][18] = tree.jet1_y_bord
	InputData[ientry][19] = tree.jet2_y_bord
	InputData[ientry][20] = tree.jet3_y_bord
	InputData[ientry][21] = tree.jet4_y_bord

	InputData[ientry][22] = tree.lep1_eta
	InputData[ientry][23] = tree.lep1_phi
	InputData[ientry][24] = tree.lep1_pt
	InputData[ientry][25] = tree.met
	InputData[ientry][26] = tree.met_phi
	InputData[ientry][27] = tree.mH
        InputData[ientry][28] = tree.mbb_leading_bjets
	InputData[ientry][29] = tree.mbb_maxdr
	InputData[ientry][30] = tree.mbb_mindr
        

np.save('InputData.npy', InputData)
#print InputData[:][28]
#print InputData[:][29]
#print InputData[:][30]
#
from sklearn.externals import joblib
scaler = joblib.load('model_FCNC_mparam/scaler.pkl')

#InputData=np.load('InputData_ttbarlightP7.npy')
#print InputData

InputData_transfered = scaler.transform(InputData)



from keras.models import load_model

test_model = load_model('model_FCNC_mparam/model.h5')
test_model.summary()

test_model.compile(loss='binary_crossentropy', optimizer='adam' ,metrics=['accuracy'])

scores = test_model.predict(InputData_transfered, batch_size=1028)


score = array( 'f', [ 0.] )
print scores
#score = 0.

new_branch = tree_new.Branch( 'scores', score, 'scores/F' )

for i_score in range(0, len(scores)-1): 
    score[0] = scores[i_score][0]
    new_branch.Fill()
    
tree_new.Write("", ROOT.TObject.kOverwrite)

file.Write()
file.Close()

