import os, sys
import ROOT
import numpy as np
import h5py
import math

from array import array
from sklearn.preprocessing import StandardScaler,LabelEncoder

inputFile = '/eos/atlas/user/p/pengc/NN_Output_27_Mar_2019/ttbarbb.root'
N_fold = 3

def CVSplit(Input, n):
    CV_samples=[]
    for i in range(0, n):
        CV_samples.append(Input[i::n])
    
    return CV_samples	

def CVMerge(arr):
    new_arr = []
    for i_event in range(0, len(arr[0])):
        for i_arr in range(0, len(arr) ):
            if i_event<len(arr[i_arr]):
                new_arr.append(arr[i_arr][i_event])
	
    return new_arr


file = ROOT.TFile(inputFile, 'update')

tree = file.Get('tree')
tree_new = file.Get('tree')

NEntries = tree.GetEntries()


print ('Total Entries = ' + str(NEntries))

InputVariables = ['jet0_btagw_bord', 'jet1_btagw_bord', 'jet2_btagw_bord', 'jet3_btagw_bord', 'jet4_btagw_bord',
		  'jet0_m_bord', 'jet1_m_bord','jet2_m_bord', 'jet3_m_bord', 'jet4_m_bord',
		  'jet0_phi_bord', 'jet1_phi_bord', 'jet2_phi_bord','jet3_phi_bord', 'jet4_phi_bord',
		  'jet0_pt_bord', 'jet1_pt_bord','jet2_pt_bord', 'jet3_pt_bord', 'jet4_pt_bord',
		  'jet0_y_bord', 'jet1_y_bord', 'jet2_y_bord', 'jet3_y_bord', 'jet4_y_bord',
		  'lep1_eta', 'lep1_phi', 'lep1_pt', 'met', 'met_phi']

OtherVariables = ['jets_n', 'bjets_n', 'nomWeight_weight_btag', 'nomWeight_weight_jvt', 'nomWeight_weight_leptonSF', 'nomWeight_weight_mc', 'nomWeight_weight_norm', 'nomWeight_weight_pu'] 

AllVariables = InputVariables+OtherVariables

origtree = tree

tree.SetBranchStatus("*",0)

# -- initiating the relevant branches only ----- #
for ivar in AllVariables : 
	tree.SetBranchStatus(ivar, 1)

InputData = np.zeros(shape=(NEntries, len(InputVariables) ))

# -- storing the tree in numpy array -- #

decade = 0
for ientry in range(NEntries) :

	progress = 10.0*ientry / (1*NEntries)
	p = math.floor(progress)
	if(p > decade) : 
		print ( str(10*p) + '%')
	decade = p

	tree.GetEntry(ientry)

	InputData[ientry][0] = tree.jet0_btagw_bord
	InputData[ientry][1] = tree.jet1_btagw_bord
	InputData[ientry][2] = tree.jet2_btagw_bord
	InputData[ientry][3] = tree.jet3_btagw_bord
	InputData[ientry][4] = tree.jet4_btagw_bord

	InputData[ientry][5] = tree.jet0_m_bord
	InputData[ientry][6] = tree.jet1_m_bord
	InputData[ientry][7] = tree.jet2_m_bord
	InputData[ientry][8] = tree.jet3_m_bord
	InputData[ientry][9] = tree.jet4_m_bord

	InputData[ientry][10] = tree.jet0_phi_bord
	InputData[ientry][11] = tree.jet1_phi_bord
	InputData[ientry][12] = tree.jet2_phi_bord
	InputData[ientry][13] = tree.jet3_phi_bord
	InputData[ientry][14] = tree.jet4_phi_bord

	InputData[ientry][15] = tree.jet0_pt_bord
	InputData[ientry][16] = tree.jet1_pt_bord
	InputData[ientry][17] = tree.jet2_pt_bord
	InputData[ientry][18] = tree.jet3_pt_bord
	InputData[ientry][19] = tree.jet4_pt_bord

	InputData[ientry][20] = tree.jet0_y_bord
	InputData[ientry][21] = tree.jet1_y_bord
	InputData[ientry][22] = tree.jet2_y_bord
	InputData[ientry][23] = tree.jet3_y_bord
	InputData[ientry][24] = tree.jet4_y_bord

	InputData[ientry][25] = tree.lep1_eta
	InputData[ientry][26] = tree.lep1_phi
	InputData[ientry][27] = tree.lep1_pt
	InputData[ientry][28] = tree.met
	InputData[ientry][29] = tree.met_phi


np.save('InputData.npy', InputData)
print InputData

scores_arr=[]
InputData_cv=CVSplit(InputData, N_fold)

for i in range(0, N_fold):
    if i == 0: 
        foldsurfix = '_fold%s' %(N_fold-1)
    else: 
        foldsurfix = '_fold%s' %(i+1)    
    
    print 'Applying '+foldsurfix, 'Number of Events: ', InputData_cv[i].shape

    from sklearn.externals import joblib
    scaler = joblib.load('model_cv_test/scaler'+foldsurfix+'.pkl')
    InputData_transfered = scaler.transform(InputData_cv[i])

    from keras.models import load_model

    test_model = load_model('model_cv_test/model'+foldsurfix+'.h5')
    test_model.summary()
    test_model.compile(loss='binary_crossentropy', optimizer='adam' ,metrics=['accuracy'])

    scores_tmp = test_model.predict(InputData_transfered, batch_size=512)
    scores_arr.append(scores_tmp)

scores = CVMerge(scores_arr)

score = array( 'f', [ 0.] )
#print scores
#score = 0.

new_branch = tree_new.Branch( 'scores', score, 'scores/F' )

for i_score in range(0, len(scores)-1): 
    score[0] = scores[i_score][0]
    new_branch.Fill()
    
tree_new.Write("", ROOT.TObject.kOverwrite)

file.Write()
file.Close()



