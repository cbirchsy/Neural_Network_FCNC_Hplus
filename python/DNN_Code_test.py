#!/usr/bin/env python
from optparse import OptionParser
import os
import sys

sys.path.insert(2, '/afs/cern.ch/work/p/pengc/miniconda3/envs/DNNENV/lib/python2.7/site-packages')
print 'PYTHONPATH', sys.path

import numpy as np
import pandas as pd

import keras

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score, classification_report
from prettytable import PrettyTable

#import matplotlib
#import matplotlib.pyplot as plt

import nn_model
#import data_visualization
import load_data
from nn_config import NNConfig
from EarlyStop import EarlyStoppingbyOT
import utils

os.environ['QT_QPA_PLATFORM']='offscreen'
#****************************************
#Adding Options when runing the code. 
#****************************************
def get_comma_separated_args(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

def get_comma_separated_args_float(option, opt, value, parser):
    setattr(parser.values, option.dest, map(float, value.split(',')))

def get_comma_separated_args_int(option, opt, value, parser):
    setattr(parser.values, option.dest, map(int, value.split(',')))

#######################################
parser = OptionParser()
parser.add_option('-s', '--SigList',
                  type='string',
                  help='list of signal, separate with comma, no blanck space', 
                  action='callback',
                  callback=get_comma_separated_args,
                  dest = "SigList")
parser.add_option('-b', '--BkgList',
                  type='string',
                  help='list of background, separate with comma, no blanck space',
                  action='callback',
                  callback=get_comma_separated_args,
                  dest = "BkgList")
parser.add_option('-p', '--filepath',
                  type='string',
                  help='Input File path, default to /eos/atlas/user/p/pengc/FCNC-Analysis_output_fcnc_NNsinput_round1_vhfsplitted0_merged/',
                  default='/eos/atlas/user/p/pengc/FCNC-Analysis_output_fcnc_NNsinput_round1_vhfsplitted0_merged/',)
#########
parser.add_option("--do_plots", action="store_true", dest="do_plots", help="do variable plots, True or False, using default set to false if without -r" , default=False)
parser.add_option("--plot_dir", dest="plot_dir", type='string', help='directory perfix to store plots', default="plots")
parser.add_option("-f", "--files_dir", dest="files_dir", type='string', help='Files saved folder', default="Files")
#########
parser.add_option("--layers", dest="layers", type='string', help="number of hidden layers", action='callback',  callback=get_comma_separated_args_int)
parser.add_option("--nodes", dest="nodes", type='string', help="number of nodes in each layer", action='callback',  callback=get_comma_separated_args_int)
parser.add_option("--activation", dest="ACTIVATION", type='string', help="activation function", default='relu')
parser.add_option("--lossfunc", dest="lossfunc", type='string', help="loss function", default='categorical_crossentropy')
parser.add_option("--lr", dest="LR", type='string',help="list of learning rate separated by comma", action='callback', callback=get_comma_separated_args_float)
parser.add_option("--do_batchnorm", dest="do_BN", action="store_false", help="Use batch normalizetion", default=True)
parser.add_option("--dropout", dest='DropOut', type='string', help='Drop out', action='callback', callback=get_comma_separated_args_float)
parser.add_option("--optimazer", dest="OPTIMAZER", help="optimazer used to minimaze loss function, See keras documentation", type='string', default='adam')
parser.add_option("--batchsize", dest="BatchSize", type='string', help="Batch_size", action='callback', callback=get_comma_separated_args_int)
parser.add_option("--classes", dest="Classes", type='int', help="Integer, Number of classes. Value 1 for binnary classification", default = 1)
parser.add_option("--cuts", dest="Cuts", type='string', help='list of cuts, separte with comma, no blanck space, same for signal and background', action='callback',callback=get_comma_separated_args,)
parser.add_option("--n_fold", dest='N_Fold', type=int, help="number of cross validation fold, default value 2", default=2)
parser.add_option("--inputvariables", dest="Inputvariables", type='string', help="text file where saved input variables", default='Input_Features.txt')
(options, args) = parser.parse_args()

#****************************************
#Data Processing: Getting signal and background sample
#****************************************

InputVariables=utils.Read_InputVariables(options.Inputvariables)
print 'Input Variables: ', InputVariables

if options.SigList == None: 
    siglist=['uHbW']
else:
    siglist=options.SigList
    
if options.BkgList == None: 
    bkglist=['ttbarbb']
else:
    bkglist=options.BkgList
files_folder=options.files_dir+"_"+siglist[0]

#load data and add labels
number_of_classes=options.Classes
n_fold=options.N_Fold
#
##Data Ploting (Distribution, correletion, scatter plot)
#if options.do_plots:
#   print "==> Ploting variable distribution, correlations and scatter plots"
#   plot_dir=options.plot_dir+"_"+siglist[0]
#   print "  --> creating output plot folder " + plot_dir
#   os.system("mkdir -p " + plot_dir)
#   for InputVariable in InputVariables:
#       data_visualization.plot_distribution(plot_dir, InputVariable, TotalPD_bkg, TotalPD_sig, 'ttbar', 'Hpluscb' )
#   data_visualization.plot_correlations(TotalPD_sig, InputVariables, plot_dir, 'Corelations_S')
#   data_visualization.plot_correlations(TotalPD_bkg, InputVariables, plot_dir, 'Corelations_B')
#   #data_visualization.plot_scatter(TotalPD_sig, InputVariables, plot_dir,'Scatter_S', 4)
#   #data_visualization.plot_scatter(TotalPD_bkg, InputVariables, plot_dir,'Scatter_B', 4)
#

if options.Cuts==None:
    cuts=["jets_n>3", "bjets_n>1"]
else:
    cuts=options.Cuts


if n_fold==1:
    total_pd=load_data.load_data(siglist, bkglist, options.filepath, number_of_classes, False, cuts)
else: 
    total_pd=load_data.load_data_CV(siglist, bkglist, options.filepath, number_of_classes, n_fold, False, cuts)
#****************************************
#create X array and y array, split train and 
#****************************************

for ith_fold in range(0, n_fold):

    X_train_ori, y_train_tmp, X_val_ori, y_val_tmp, X_test_ori, y_test_tmp = load_data.train_test_pre(total_pd, ith_fold, InputVariables, 'label')
    #Fit transform on X:
    X_train_tmp=X_train_ori[InputVariables].values
    X_val_tmp=X_val_ori[InputVariables].values
    X_test_tmp=X_test_ori[InputVariables].values
    
    scaler = StandardScaler().fit(X_train_tmp)
    # Save the scaler to .pkl file
    os.system("mkdir -p " + files_folder)
    from sklearn.externals import joblib
    joblib.dump(scaler , files_folder+'/scaler_fold%s.pkl' %ith_fold) 
    loaded_scaler = joblib.load(files_folder+'/scaler_fold%s.pkl' %ith_fold)
    
    X_train=scaler.transform(X_train_tmp)
    X_val=scaler.transform(X_val_tmp)
    X_test=scaler.transform(X_test_tmp)
    X_val, X_test
    # Convert labels to categorical one-hot encoding
    if number_of_classes>1:
        y_train=keras.utils.to_categorical(y_train_tmp, num_classes=number_of_classes)
        y_val=keras.utils.to_categorical(y_val_tmp, num_classes=number_of_classes)
        y_test=keras.utils.to_categorical(y_test_tmp, num_classes=number_of_classes)
    else: 
        y_train=y_train_tmp
        y_val=y_val_tmp
        y_test=y_test_tmp
    #****************************************
    # Build DNN
    #****************************************
    #config DNN model
    n_dim=X_train.shape[1]
    if options.nodes==None:
        n_nodes = [32]
    else:
        n_nodes = options.nodes
    if options.layers==None:
        n_depth = [2]
    else:
        n_depth=options.layers
    activation=options.ACTIVATION
    do_bn=options.do_BN
    activation_f='sigmoid'
    if options.DropOut==None:
        dropouts=[0.5]
    else:
        dropouts=options.DropOut
    optimizer=options.OPTIMAZER
    if options.LR==None:
        lrs=[0.001]
    else:
        lrs = options.LR
    if options.BatchSize==None:
        batchsizes=[128]
    else:
        batchsizes = options.BatchSize
    
    lossfunc=options.lossfunc
    NN_configs=[]
    #for i in range(0,1):
    #    depth=np.random.randint(2,6)
    #    nodes=np.random.randint(70,200)
    #    dropout=0.5
    #    batchsize=2**np.random.randint(8,12)
    #    lr = (np.random.rand()*9+1)*(10**np.random.randint(-5,-2))
        #depth=2
        #nodes=150
        #dropout=0.5
        #batchsize=1024
        #lr = 0.001

    for lr in lrs:
        for batchsize in batchsizes:
            for dropout in dropouts:
                for nodes in n_nodes:
                    for depth in n_depth: 
                        config = NNConfig(n_dim, depth, nodes, lr, batchsize, dropout, activation, activation_f, lossfunc, optimizer, 10, 10, number_of_classes, MN=3.)
                        NN_configs.append(config)
    #config = NNConfig(n_dim, depth, nodes, lr, batchsize, dropout, activation, activation_f, lossfunc, optimizer, 300, 20, number_of_classes, MN=3.)
    #                   NN_configs.append(config)
    print "Total models %s" %len(NN_configs)
    #################################################
    for config in NN_configs:
        plotsuffix='_depth%s_nodes%s_lr%s_bs%s_drop%s' %(config.n_depth, config.n_nodes, config.lr, config.batchsize, config.dropout)
        modelfolder=files_folder+'/model'+plotsuffix
        os.system("mkdir -p " + modelfolder)
        lossplotname=modelfolder+'/Loss_fold%s.png' %(ith_fold+1)
        accplotname=modelfolder+'/Accuracy_fold%s.png' %(ith_fold+1)
        modelname=modelfolder+'/model_fold%s.h5' %(ith_fold+1)
        model = config.BuildDNNmodel()
        model.summary()     
        #from keras.utils import plot_model
        #plot_model(model, to_file='model.eps')
    
        optimizer = config.SetOptimizer()
        model.compile(loss=lossfunc, optimizer=optimizer ,metrics=['accuracy'])
        callbacks = config.SetCallBacks(False, modelname)
        # Start the training!                                                                                                                                                                                  
        modelMetricsHistory = model.fit(X_train, y_train, epochs=300, batch_size=config.batchsize, validation_data=(X_val, y_val),callbacks=callbacks, verbose=0)
    
        perf_1 = model.evaluate(X_train, y_train, batch_size=config.batchsize)
        perf_2 = model.evaluate(X_test, y_test, batch_size=config.batchsize)
        testLoss = '%0.3f(%0.3f)' % (perf_2[0], perf_1[0])
        testAccuracy = '%0.3f(%0.3f)' % (perf_2[1], perf_1[1])
        #summarize history for accuracy                                                                                                                                                                
        np.save(modelfolder+'/acc_fold%s' %(ith_fold+1), modelMetricsHistory.history['acc'])
        np.save(modelfolder+'/val_acc_fold%s' %(ith_fold+1), modelMetricsHistory.history['val_acc'])
        #plt.plot(modelMetricsHistory.history['acc'])
        #plt.plot(modelMetricsHistory.history['val_acc'])
        #plt.title('Model accuracy')
        #plt.ylabel('Accuracy')
        #plt.xlabel('Epoch')
        #plt.legend(['Train', 'Val'], loc='lower right')
        #plt.figtext(0.5, 0.5, testAccuracy, wrap=True, horizontalalignment='center', fontsize=10)
        #plt.savefig(accplotname)
        #plt.clf()
        # summarize history for loss
        np.save(modelfolder+'/loss_fold%s' %(ith_fold+1), modelMetricsHistory.history['loss'])
        np.save(modelfolder+'/val_loss_fold%s' %(ith_fold+1), modelMetricsHistory.history['val_loss'])
        
        #plt.plot(modelMetricsHistory.history['loss'])
        #plt.plot(modelMetricsHistory.history['val_loss'])
        #plt.title('Model loss')
        #plt.ylabel('Loss')
        #plt.xlabel('Epoch')
        #plt.legend(['Train', 'Val'], loc='upper right')
        #plt.figtext(0.5, 0.5, testLoss, wrap=True, horizontalalignment='center', fontsize=10)
        #plt.savefig(lossplotname)
        #plt.clf()
        # Generates output predictions for the input samples.                                                                                                                                                       
        #yhat_test = model.predict(X_val,batch_size=config.batchsize)
        yhat_test=model.predict(X_test, batch_size=config.batchsize)
        yhat_train=model.predict(X_train, batch_size=config.batchsize)
        #print yhat_test, yhat_train
        #if number_of_classes > 1:
        #    yhat_val=utils.ScoresComputer(yhat_val)
        #    yhat_train=utils.ScoresComputer(yhat_train)    
        foldname='fold%s' %(ith_fold+1)
        #Save relevant variables in a root TTree

#        np.save(modelfolder+'/yhat_val'+foldname, yhat_val)
#        np.save(modelfolder+'/y_val'+foldname, y_val)
#        np.save(modelfolder+'/yhat_train'+foldname, yhat_train)
#        np.save(modelfolder+'/y_train' + foldname, y_train)
        rootfile = "tree"+foldname+".root"
        utils.Save_in_Tree(X_train_ori[['jets_n', 'bjets_n']], X_test_ori[['jets_n', 'bjets_n']], y_train_tmp, y_test_tmp, yhat_train, yhat_test, modelfolder, rootfile)
        if number_of_classes == 1:
            fpr, tpr, thresholds = roc_curve(y_test, yhat_test) 
            roc_auc  = auc(fpr, tpr)
            tnr = 1-fpr
            config.SetPerformance(loss=testLoss, accuracy=testAccuracy, ROCI=round(roc_auc, 3))   
            #plt.plot(tpr, tnr, color='black',  lw=2, label='Testing, ROC curve (area = %0.3f)' % roc_auc)
            #plt.plot([0, 1], [1, 0], color='navy', lw=2, linestyle='--')
            #plt.xlim([-0.05, 1.0])
            #plt.ylim([0.0, 1.05])
            #plt.ylabel('Background rejection')
            #plt.xlabel('Signal efficiency')
            #plt.title('ROC curves for Background rejection vs Signal')
        #plt.plot([0.038, 0.038], [0,1], color='red', lw=1, linestyle='--') # same background rejection point                                                                                                 
            #plt.legend(loc="lower right")
            #plt.savefig(modelfolder+"/ROC"+foldname+".pdf")
            #plt.clf()
        else:
            config.SetPerformance(loss=testLoss, accuracy=testAccuracy, ROCI=1)#round(roc_auc, 3))   
        config.Show()
    
        
        def split_classifer(y_true, y_prob):
            index = 0
            y_prob_bkg=[]
            y_prob_sig=[]
            for i in y_true:
                if i==0: #bkgevent                                                                                                                                                                                
                   y_prob_bkg.append(y_prob[index,0])
                elif i==1: #signalevent                                                                                                                                                                          
                   y_prob_sig.append(y_prob[index,0])
                else:
                   print 'error'
                index = index+1
            return y_prob_sig, y_prob_bkg
        
        #ysig, ybkg=split_classifer(y_val, yhat_val)
        
        #bins = np.linspace(0,1,21)
        #plt.hist(ysig, histtype='step', density=True, bins=bins, label='$uHcW$', linewidth=2)
        #plt.hist(ybkg, histtype='step', density=True, bins=bins, label='$ttbar$', linewidth=2)
        #plt.xlabel('Discriminant')
        #plt.ylabel('Abitrary Units')
        #plt.legend(loc='best')
        #plt.savefig(modelfolder+'/Discriminant.png')
        #plt.clf()
    
    NN_configs.sort(cmp=None, key=lambda x:x.ROCI, reverse=True)
    
    for i in range(0, min(len(NN_configs), 30)):
        print '==> Performance No.%s configration:' %(i+1)
        NN_configs[i].Show()
    
print "==>Done!"
