import numpy as np
import pandas as pd
from root_numpy import array2tree, array2root
import os,sys

def ScoresComputer(ps):
    P_s=ps[:,0]
    P_ttbb=ps[:,1]
    P_ttcc=ps[:,2]
    #p_ttlight=[:,3]
    p_total=P_s+1.2*P_ttbb+1.2*P_ttcc #used for test only
    
    scores=P_s/p_total
    return scores

def Save_in_Tree(X_train, X_val, y_train, y_val, yhat_train, yhat_val, path, rootfile): 
    #X should be pandas dataframe. 
    #save X_train 
    
    if yhat_train.shape[1]==4:
        print "In stroing p_sig in root Tree: %s , %s" %(y_train.shape[0], y_val.shape[0])
        P_sig_train=yhat_train[:,0]
        P_ttbb_train=yhat_train[:,1]
        P_ttcc_train=yhat_train[:,2]
        P_ttlight_train=yhat_train[:,3]
        
        x=[]
        for i in range(0,P_sig_train.shape[0]):
            p_sig_train=(P_sig_train[i],)
            p_ttbb_train=(P_ttbb_train[i],)
            p_ttcc_train=(P_ttcc_train[i],)
            p_ttlight_train=(P_ttlight_train[i],)
            p_sig_train_truth=(y_train[i],)
            x.append(p_sig_train_truth+p_sig_train+p_ttbb_train+p_ttcc_train+p_ttlight_train)
        
        a = np.array(x,  dtype=[('label', 'int'), ('p_sig', 'float32'), ('p_ttbb', 'float32'), ('p_ttcc', 'float32'), ('p_ttlight', 'float32')])
        array2root(a, path+'/tree.root', 'train_tree', 'recreate')
        
        P_sig_val=yhat_val[:,0]
        P_ttbb_val=yhat_val[:,1]
        P_ttcc_val=yhat_val[:,2]
        P_ttlight_val=yhat_val[:,3]
        
        x_val=[]
        for i in range(0,P_sig_val.shape[0]):
            p_sig_val=(P_sig_val[i],)
            p_ttbb_val=(P_ttbb_val[i],)
            p_ttcc_val=(P_ttcc_val[i],)
            p_ttlight_val=(P_ttlight_val[i],)
            p_sig_val_truth=(y_val[i],)
            x_val.append(p_sig_val_truth+p_sig_val+p_ttbb_val+p_ttcc_val+p_ttlight_val)
        
        b = np.array(x_val,  dtype=[('label', 'int'), ('p_sig', 'float32'), ('p_ttbb', 'float32'), ('p_ttcc', 'float32'), ('p_ttlight', 'float32')])
        array2root(b, path+'/tree.root', 'val_tree')
    elif yhat_train.shape[1]==1:
        print "In storing NN score in root tree: %s, %s" %(yhat_train.shape[0], yhat_val.shape[0]) 
        NN_score_train=yhat_train[:,0]
        x=[]
        print NN_score_train.shape[0]
        X_train.loc[:,'Discriminant']=NN_score_train
        X_train.loc[:,'label']=y_train

#        for i in range(0,NN_score_train.shape[0]):
#            NN_truth=(y_train[i],)
#            Discriminate=(NN_score_train[i],)
#            x.append(NN_truth+Discriminate)
#        a = np.array(x, dtype=[('label', 'int'), ('Discriminate', 'float32')])
#        array2root(a, path+'/tree.root', 'train_tree', 'recreate')
        
        NN_score_val=yhat_val[:,0]
        X_val.loc[:,'Discriminant']=NN_score_val
        X_val.loc[:,'label']=y_val 
        
        arr_train = X_train.to_records(index=False)
        arr_val = X_val.to_records(index=False)
        arr_train, arr_val
    
        array2root(arr_train, path+'/'+rootfile, 'train_tree', 'recreate')
        array2root(arr_val, path+'/'+rootfile, 'val_tree')
        print "Done!"
#        x_val=[]
#        for i in range(0,NN_score_val.shape[0]):
#            NN_truth=(y_val[i],)
#            Discriminate=(NN_score_val[i],)
#            x_val.append(NN_truth+Discriminate)
#        b = np.array(x_val, dtype=[('label', 'int'), ('Discriminate', 'float32')])
#        array2root(b, path+'/tree.root', 'val_tree')
def Read_InputVariables(Input_txtfile='Input_Features.txt'):
    Input_Variables=[]
    if not os.path.exists(Input_txtfile):
        print "Error:%s not found!" %Input_txtfile
        sys.exit(1)
    else:
        with open(Input_txtfile) as f:
            for line in f:
                if not len(line.split())==1:
                    print "Warning: Format warning, please make sure only one variable in a line"
                Input_Variables.append(line.split()[0])

    return Input_Variables
        
