#!/usr/bin/env python
import os, re

from optparse import OptionParser

import numpy as np
import pandas as pd
from root_numpy import root2array
from numpy.lib.recfunctions import stack_arrays
import glob

#****************************************
#Adding options when running the code.
#****************************************
parser = OptionParser()
parser.add_option('-i', "--infile" , help="input root file", default="/eos/")
parser.add_option('-o', "--outfile", help="output .pkl file name", default='NewFile.pkl')
parser.add_option('-t', "--treename", help="tree name", default='tree')
parser.add_option('-s', action="store_true", help="True for signal", dest="IsSig", default=True)
parser.add_option('-b', action="store_false", help="False for bkg", dest="IsSig", default=False)
#paeser.add_option('-w', "--weight", help="Total weight", default='nomWeight_weight_muon_trigger')

#****************************************
(options, args) = parser.parse_args()

def root2pandas(files_path, tree_name, **kwargs):
    '''
    Args:
    -----
        files_path: a string like './data/*.root', for example
        tree_name: a string like 'Collection_Tree' corresponding to the name of the folder inside the root 
                   file that we want to open
        kwargs: arguments taken by root2array, such as branches to consider, start, stop, step, etc
    Returns:
    --------
        output_panda: a pandas dataframe in which all the info from the root file will be stored

    Note:
    -----
        if you are working with .root files that contain different branches, you might have to mask your data
        in that case, return pd.DataFrame(ss.data)
    '''
    # -- create list of .root files to process
    files = glob.glob(files_path)

    # -- process ntuples into rec arrays
    ss = stack_arrays([root2array(fpath, tree_name, **kwargs).view(np.recarray) for fpath in files])

    try:
        return pd.DataFrame(ss)
    except Exception:
        return pd.DataFrame(ss.data)

df = root2pandas(options.infile, options.treename)

#****************************************
#Adding Issig to the df, 1 for signal events and 0 for bkg events
#****************************************
#if options.IsSig:
#   print '===> Adding IsSig to signal sample'
#   df['IsSig'] = np.ones((df.shape[0]), dtype=int)
#else:
#   print '===> Adding IsSig to bkg sample'
#   df['IsSig'] = np.zeros((df.shape[0]), dtype=int)

#****************************************
#Adding Full Events weight to df
#****************************************
def SetTotalWeight(*args):
    TotalWeight=np.ones((df.shape[0]), dtype=int)
    math_weight='1'
    for weight in args:
        math_weight=math_weight+'*'+weight
        TotalWeight=TotalWeight*df[weight]
    print 'total weight = '+math_weight
    return TotalWeight

#Total_weight=SetTotalWeight('nomWeight_weight_muon_trigger', 'nomWeight_weight_pu', 'nomWeight_weight_btag', 'nomWeight_weight_elec', 'nomWeight_weight_elec_trigger', 'nomWeight_weight_jvt', 'nomWeight_weight_mc', 'nomWeight_weight_muon', 'nomWeight_weight_norm',)

##Total_weight=SetTotalWeight('nomWeight_weight_muon_trigger',)
#df['FullEventsWeight']=Total_weight

#****************************************
#wrighting pkl files
#****************************************

out_file=options.outfile
df.to_pickle(out_file)

print "=== wrote pkl file %s\n" % options.outfile
print "=== done!"
