Neural Network for enents classification
========================================
Setup python enviroments on lxplus(recommond) or local machine
----------------------------------------
Use the approach of first installing a package manager like [Miniconda][miniconda] or [Anaconda][anaconda], Installation instructions are available on the Machine Learning Forum [Twiki][MLTwiki]:
```
Download minconda package: wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
Install minconda: sh Minconda-latest-Linux-x86_64.sh
Make sure you have PATH="<path>/miniconda3/bin:$PATH" in your ~/.bash_profile
Add installation channels: conda config --add channels https://conda.anaconda.org/NLeSC
To create a Conda environment simply type: conda create --name=DNNENV
To activate this environment: source activate DNNENV
Install the needed packages: conda install conda install root=6 python=2 mkl jupyter numpy scipy matplotlib scikit-learn h5py rootpy root-numpy pandas scikit-image theano pydot
pip install keras prettytable
```

Troubleshooting
-------------------------------------------
`CXXABI_1.3.9 not included in libstdc++.so.6` problem occurs when install `keras` and `rootpy` in same enviroment. [Solution][libSo]

    1. cd <Installation path>/miniconda3/envs/<nameoftheenvironmet>/lib
    2. ls libstdc++.so*  here you should have **libstdc++.so**, **libstdc++.so.6** and at least one of **libstdc++.so.6.0.24** or **libstdc++.so.6.0.25** 
    3. rm **libstdc++.so** && rm **libstdc++.so.6**
    4. ln -s **libstdc++.so.6.0.25** **libstdc++.so.6** && ln -s **libstdc++.so.6.0.25** **libstdc++.so**

Usage
-------------------------------------------
The main script, `DNN_code_test.py`, takes [pandas dataframe][PD] formate `.pkl` files as input. Before starting training, make sure you have proper formate of data sample.\
To transfer `.root` files to  `.pkl` files, one can use `data_preparation/Prepare_data.py`. 

    python data_preparation/Prepare_data.py -i <Full path of root file>/ttbarbb.root -o ttbarbb.pkl -b(for adding labels for bkg)
    python data_preparation/Prepare_data.py -i <Full path of root file>/uHbW.root -o uHbW.pkl -s(for adding labels for sig)

For using `DNN_code_test.py`, please firstly check options you can use `python DNN_code_test.py -h`.  \
NNs can be trained both in your local lxplus mechine(sh run_Hplus.sh) and submit to CERN batch system via conda (condor_submit python/Hplus.sub).

Options
--------------------------------------------
    
    -s or --SigList: list of signal .pkl files. Separated by comma, no blanck space. Examples: Hplus_cb_m100 or uHbW
    -b or --BkgList: list of background .pkl files. Separated by comma, no blanck space. Default: ttbarcc,ttbarb,ttbarlight
    -p or --filepath: Input .pkl file path. Default: '/eos/atlas/user/p/pengc/Hplus_Data_pkl_v4/' 
    -f or --files_dir: Prefix of all output saves folder. Default: 'Files'
    --activation: Activation function between each hidden layer. Default: 'relu' 
    --lossfunc: Loss function. Example: 'categorical_crossentropy' for multiclass classification 'binary_crossentropy' for binary classfication. 
    --do_batchnorm: Use batch normalizetion. Default: True
    --optimazer: Optimazer. Default: 'adam'
    --classes: Number of classes. Value 1 for binnary classification. Number of classes should be 1 or equal to number of signals + number of backgrounds or equal to 1+number of backgrounds/number of signals. 
    --cuts: Cuts on signal sample and background sample. Same treadment for signal sample and background samlpe. Support '>', '<', '='. Not Support '>=' and '<='. Example: 'jets_n=5,bjets_n=3'
    --n_fold: Number of fold for cross training.
    --layers: Hyperparameter. Number of hidden layers. Can use a list of integers for grid scan. Example: 2,3,4
    --nodes: Hyperparamter. Number of neurons in each hidden layer. Can use a list of integers for grid scan. Example: 32,46,58
    --lr: Hyperparameter. Learning rate. Example 0.007,0.004,0.001,0.0007,0.0004
    --dropout: Hyperparameter. Dropout. Example: 0.2,0.4
    --batchsize: Hyperparameter. Batch size. Example:128,256,512
    --inputvariables: Textfile where list inputvariables, see python/InputFeature.txt
    

[miniconda]:https://conda.io/miniconda.html
[anaconda]:https://www.anaconda.com/ 
[MLTwiki]:https://twiki.cern.ch/twiki/bin/view/AtlasComputing/MLSoftwareStandAloneSetup/
[libSo]:https://github.com/ContinuumIO/anaconda-issues/issues/5191#issuecomment-368243432
[PD]:https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.DataFrame.html
